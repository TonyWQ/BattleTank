// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"



ATank * ATankAIController::GetControllerTank() const {
	return Cast<ATank>(GetPawn());
}

ATank *ATankAIController::GetPlayerTank() const {
	auto PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
	if (!PlayerPawn)
	{
		return nullptr;
	}
	return Cast<ATank>(PlayerPawn);
}

void ATankAIController::BeginPlay() { 
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("AIController Begin Play"));

	auto ControlTank = GetPlayerTank();
	if (!ControlTank)
	{
		UE_LOG(LogTemp, Warning, TEXT("AIController not find player tank"));
	}
	else
	{
		//UE_LOG(LogTemp, Warning, TEXT("PlayerController Begin Play"));
		UE_LOG(LogTemp, Warning, TEXT("AIController found player tank : %s"), *(ControlTank->GetName()));
	}

	//auto ControlTank = GetControllerTank();
	//if (!ControlTank)
	//{
	//	UE_LOG(LogTemp, Warning, TEXT("AIController not process"));
	//}
	//else
	//{
	//	//UE_LOG(LogTemp, Warning, TEXT("PlayerController Begin Play"));
	//	UE_LOG(LogTemp, Warning, TEXT("AIController possessing : %s"), *(ControlTank->GetName()));
	//}
}


void ATankAIController::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	 
}

void ATankAIController::AimTowardsCrosshair() {

}